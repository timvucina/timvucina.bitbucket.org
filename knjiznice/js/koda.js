
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var x;
var cntry = '';
var lifeLeft;
/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generate(){
	console.log("generate is alive");
	for(var i = 1; i < 4; i++){
		generirajPodatke(i, function(j, ehr){
			console.log(ehr);
			console.log(j);
			switch (j) {
				case 1:
					$("#1").val( ehr );
					break;
		
				case 2:
					$("#2").val( ehr );
					break;
				
				case 3:
					$("#3").val( ehr );
					break;
			}
		});
	}
}
 
function generirajPodatke(stPacienta, callback) {
	console.log("generirajPodatke is alive");
	console.log(stPacienta);
	var val;
	var ehr;
	switch (stPacienta) {
		case 1:
			val = "Snoop;Dogg;MALE;United States;1971-10-20;193;85;36.7;118;92;67";
			break;
		
		case 2:
			val = "Gloria;Borger;FEMALE;Sri Lanka;1969-06-09;169;65;37.5;140;80;121";
			break;
			
		case 3:
			val = "Dejan;Lavbic;MALE;Slovenia;1980-04-04;171;73;35.3;110;70;191";
			break;
	}
	
    var podatki = val.split(";");
    
    sessionId = getSessionId();
    
    var ime = podatki[0]; // pridobimo ime iz polja z ID-jem kreirajIme
	var priimek = podatki[1];
	var spol = podatki[2];
	var drzava = podatki[3];
	var bday = podatki[4];
	var visina = podatki[5];
	var teza = podatki[6];
	var tem = podatki[7];
	var sistol = podatki[8];
	var diastol = podatki[9];
	var pulse = podatki[10];
	
	var ehrId;

	if (!ime || !priimek || !spol ||!drzava || !bday || !tem || !sistol || !diastol || !pulse || !visina || !teza || 
	   ime.trim().length == 0 || priimek.trim().length == 0 || visina.trim().length == 0 || teza.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr", // pošiljanje zahteve
		    type: 'POST',
		    success: function (data) {
		        ehrId = data.ehrId; // ko dobimo odgovor
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            gender: spol,
		            dateOfBirth: bday,
		            partyAdditionalInfo: [{key: "country", value: drzava}, {key: "height", value: visina}, {key: "weight", value: teza}, {key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party", // da vemo, kakšna je forma, moramo pogledati dokumentacijo na ehr scape
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {  // uspešno
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
		                    console.log("Uspešno kreiran EHR '" + ehrId + "'.");
		                    $("#preberiEHRid").val(ehrId);
		                    
		                    
		                    var datumInUra = new Date();//"2014-11-21T11:40Z";
							var telesnaVisina = visina;
							var telesnaTeza = teza;
							var telesnaTemperatura = tem;
							var sistolicniKrvniTlak = sistol;
							var diastolicniKrvniTlak = diastol;
							var utripSrca = pulse;
							var merilec = "Tim Vucina";
						
							if (!ehrId || ehrId.trim().length == 0) {
								$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
							} else {
								$.ajaxSetup({
								    headers: {"Ehr-Session": sessionId}
								});
								var podatki = { // source predloge za vital signs
									// Preview Structure: https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
								    "ctx/language": "en",
								    "ctx/territory": "SI",
								    "ctx/time": datumInUra,
								    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
								    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
								   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
								    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
								    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
								    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
								    "vital_signs/pulse:0/any_event/rate|magnitude": utripSrca
								};
								var parametriZahteve = { // klic template-a
								    "ehrId": ehrId,
								    templateId: 'Vital Signs', // template id
								    format: 'FLAT',
								    committer: merilec
								};
								$.ajax({
								    url: baseUrl + "/composition?" + $.param(parametriZahteve),
								    type: 'POST',
								    contentType: 'application/json',
								    data: JSON.stringify(podatki),
								    success: function (res) { // pošiljanje podatkov na server
								    	console.log(res.meta.href);
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "' + dodani podatki.</span>");
								    },
								    error: function(err) {
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
										console.log(JSON.parse(err.responseText).userMessage);
								    }
								});
								//console.log(ehrId);
								ehr = ehrId;
							}
		                    
		                }
		            },
		            error: function(err) {  // neuspešno
		            	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
		            	console.log(JSON.parse(err.responseText).userMessage);
		            }
		        });
		    }
		});
	}
	setTimeout(function(){
		//console.log(ehr);
		callback(stPacienta, ehr);
    }, 1000);
	//$("#crateBtn").click();
	/*
	setTimeout(function(){
		console.log($("#preberiEHRid").val());
        return $("#preberiEHRid").val();
    }, 1000);
    */
  //return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val(); // pridobimo ime iz polja z ID-jem kreirajIme
	var priimek = $("#kreirajPriimek").val();
	var spol = $("#kreirajSpol").val();
	var drzava = $("#kreirajDrzavo").val();
	var bday = $("#kreirajBday").val();
	var visina = $("#kreirajVisino").val();
	var teza = $("#kreirajTezo").val();
	var tem = $("#kreirajTem").val();
	var sistol = $("#kreirajSistol").val();
	var diastol = $("#kreirajDiastol").val();
	var pulse = $("#kreirajPulse").val();
	
	var ehrId;

	if (!ime || !priimek || !spol ||!drzava || !bday || !tem || !sistol || !diastol || !pulse || !visina || !teza || 
	   ime.trim().length == 0 || priimek.trim().length == 0 || visina.trim().length == 0 || teza.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr", // pošiljanje zahteve
		    type: 'POST',
		    success: function (data) {
		        ehrId = data.ehrId; // ko dobimo odgovor
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            gender: spol,
		            dateOfBirth: bday,
		            partyAdditionalInfo: [{key: "country", value: drzava}, {key: "height", value: visina}, {key: "weight", value: teza}, {key: "ehrId", value: ehrId}]
		        };
		        globalEhr = ehrId;
		        $.ajax({
		            url: baseUrl + "/demographics/party", // da vemo, kakšna je forma, moramo pogledati dokumentacijo na ehr scape
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {  // uspešno
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
		                    console.log("Uspešno kreiran EHR '" + ehrId + "'.");
		                    $("#preberiEHRid").val(ehrId);
		                    
		                    
		                    var datumInUra = new Date();//"2014-11-21T11:40Z";
							var telesnaVisina = visina;
							var telesnaTeza = teza;
							var telesnaTemperatura = tem;
							var sistolicniKrvniTlak = sistol;
							var diastolicniKrvniTlak = diastol;
							var utripSrca = pulse;
							var merilec = "Tim Vucina";
						
							if (!ehrId || ehrId.trim().length == 0) {
								$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
							} else {
								$.ajaxSetup({
								    headers: {"Ehr-Session": sessionId}
								});
								var podatki = { // source predloge za vital signs
									// Preview Structure: https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
								    "ctx/language": "en",
								    "ctx/territory": "SI",
								    "ctx/time": datumInUra,
								    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
								    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
								   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
								    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
								    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
								    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
								    "vital_signs/pulse:0/any_event/rate|magnitude": utripSrca
								};
								var parametriZahteve = { // klic template-a
								    "ehrId": ehrId,
								    templateId: 'Vital Signs', // template id
								    format: 'FLAT',
								    committer: merilec
								};
								$.ajax({
								    url: baseUrl + "/composition?" + $.param(parametriZahteve),
								    type: 'POST',
								    contentType: 'application/json',
								    data: JSON.stringify(podatki),
								    success: function (res) { // pošiljanje podatkov na server
								    	console.log(res.meta.href);
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "' + dodani podatki.</span>");
								    },
								    error: function(err) {
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
										console.log(JSON.parse(err.responseText).userMessage);
								    }
								});
							}
		                    
		                }
		            },
		            error: function(err) {  // neuspešno
		            	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
		            	console.log(JSON.parse(err.responseText).userMessage);
		            }
		        });
		    }
		});
	}
}

function stopTimer(x){
	console.log("clearing timer");
	clearInterval(x);
}

function makeTimer(lifeLeft){
	console.log(lifeLeft);

	if(lifeLeft && lifeLeft > 0 && lifeLeft != 6969){
		// Set the date we're counting down to
		var d = new Date();
		var year = d.getFullYear();
		var month = d.getMonth();
		var day = d.getDate();
		var c = new Date(year + lifeLeft, month, day);
		
		//var countDownDate = new Date("Sep 5, 2018 15:37:25").getTime();
		var countDownDate = c.getTime();
		
		// Update the count down every 1 second
		x = setInterval(function() {

		
		  // Get todays date and time
		  var now = new Date().getTime();
		 
		
		  // Find the distance between now an the count down date
		  var distance = countDownDate - now;
		
		  // Time calculations for days, hours, minutes and seconds
		  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		
		  // Display the result in the element with id="demo"
		  document.getElementById("timer").innerHTML = days + " days, " + hours + " hours, "
		  + minutes + " minutes, " + seconds + " seconds";
		
		  // If the count down is finished, write some text 
		  if (distance < 0) {
		    clearInterval(x);
		    document.getElementById("timer").innerHTML = "EXPIRED";
		  }
		}, 1000);
	}
	else if(lifeLeft < 0){
		document.getElementById("timer").innerHTML = "This Person Is Dead";
	}
	else if(lifeLeft == 6969){
		document.getElementById("timer").innerHTML = "Dejan Lavbic Will Never Die";
	}
	else{
		document.getElementById("bla").innerHTML = "Najprej Morate Izbrati Osebo...";
	}
}

function pokaziElement() {
	
    var x = document.getElementById("myDIV");
        x.style.display = "block";
}

function skrijElement() {
	
    var x = document.getElementById("myDIV");
        x.style.display = "none";
}

function preberiEHRodBolnika() {
    
	sessionId = getSessionId();
	
	setTimeout(function(){
        pokaziElement();
    }, 500);

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevan podatek!</span>");
	} else {
		
		var teza, visina, spol, bday, tem, sistol, diastol, puls, drzava, ime, priimek;
		 
        $.ajax({
            url: baseUrl + "/view/" + ehrId + "/body_temperature",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
            success: function (data) {
                tem = data[0].temperature;
                //console.log(tem);
            }
        });
        
        $.ajax({
            url: baseUrl + "/view/" + ehrId + "/blood_pressure",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
            success: function (data) {
                sistol = data[0].systolic;
                diastol = data[0].diastolic;
            }
        });
        
        $.ajax({
            url: baseUrl + "/view/" + ehrId + "/pulse",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
            success: function (data) {
                puls = data[0].pulse;
                //console.log(puls);
            }
        });
		
		
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {

				var party = data.party;
				spol = party.gender;
				bday = party.dateOfBirth;
				visina = party.additionalInfo.height;
				teza = party.additionalInfo.weight;
				drzava = party.additionalInfo.country;
				ime = party.firstNames;
				priimek = party.lastNames;
				
				cntry = drzava;
				
				$("#grafSmrtiH").text("Najpogostejši vzroki predčasne smrti za " + drzava);
				
				console.log(party);
				console.log(drzava);
				
				//Life Expectancy Formula:
				var age = calculateAge(new Date(bday));
				var bm = Math.abs(1 - (visina - teza - 5)/100) + 1;
				console.log(bm*bm*bm*bm);
				var vitals = Math.abs(110-sistol)/8 + Math.abs(70-diastol)/8 + Math.abs(37.5-tem)*2 + Math.pow(Math.abs(70-puls)/40, 2);
				console.log(vitals);
				var zivljenskaDoba = 90 - age/50*vitals - bm*bm*bm*bm*5;
				console.log(zivljenskaDoba);
				lifeLeft = zivljenskaDoba - age;
				
				
				if(ime == "Dejan" && priimek == "Lavbic"){
					lifeLeft = 6969;
				}
				console.log(lifeLeft);
				
				
				//$("#preberiSporocilo").html("<span class='obvestilo label label-success fade-in'>Pacient '" + party.firstNames + " " + party.lastNames + "', s tezo " + teza + " , visino " + visina + " in BMI " + patientBMI + ".</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
				console.log(JSON.parse(err.responseText).userMessage);
			}
		});
	}
}

function calculateAge(birthday) { // birthday is a date
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function najdiPovzrociteljeSmrti(str, callback){
    var topSmrti = new Array(0);
    
    var country = str.split(' ').join('-').toLowerCase();

    var url = "https://allorigins.me/get?url=" + encodeURIComponent("https://www.worldlifeexpectancy.com/") + country + "-life-expectancy" + "&callback=?";
    $.getJSON(url, function(data){
        var html = data.contents;
        html = html.replace(/<img\b[^>]*>/ig, '').replace('http://www.worldlifeexpectancy.com/subscribe-form-submit', '');
        $(html).find(".cause_table").find("td:has(a)").slice( 1 ).each(function(){
            
            var text = $(this).text();
            //var value = findValues(text, country);
            topSmrti.push({
                label: text,
                value: 0,
            });
        });
        najdiProcenteZaSmrti(topSmrti, country, callback);
    });
    //najdiProcenteZaSmrti(topSmrti, country, callback);
}
function najdiProcenteZaSmrti(topSmrti, country, callback){
    //var topSmrti = ["Coronary Heart Disease", "Stroke", "Diabetes Mellitus", "Suicide", "Asthma", "Influenza and Pneumonia", "Kidney Disease", "Lung Disease", "Liver Disease", "Alzheimers/Dementia", "Road Traffic Accidents", "Hypertension", "Breast Cancer", "Falls", "Other Injuries", "Oral Cancer", "Congenital Anomalies", "Drownings", "Lung Cancers", "Oesophagus Cancer"];
	console.log("v procentih");
    console.log(topSmrti);

    topSmrti.forEach(function(item) {

        var smrt = item.label.replace('and ','').replace('/', ' ').split(' ').join('-').toLowerCase();
        var url = "https://allorigins.me/get?url=" + encodeURIComponent("https://www.worldlifeexpectancy.com/")+ country + "-" + smrt + "&callback=?";
        
        $.getJSON(url, function(data){
            var html = data.contents;
            html = html.replace(/<img\b[^>]*>/ig, '').replace('http://www.worldlifeexpectancy.com/subscribe-form-submit', '');
            $(html).find("table.cause_table").eq( 0 ).find(".lhc_data_rank").find(".lhc_type_hidden").each(function(){
            
                var text = $(this).text();
                var num = parseFloat(text);
                item.value = num/10;
                //console.log(item);
                //console.log(url);
            });
        });
        //callback(topSmrti);
    });
    setTimeout(function(){
        callback(topSmrti);
    }, 1000);
}

$(document).ready(function() {
	document.body.style.backgroundColor = "#f2f2f2";
	
	$('#timerBtn').click(function(){
		stopTimer(x);
       makeTimer(lifeLeft);
    });

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(";");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajSpol").val(podatki[2]);
    $("#kreirajDrzavo").val(podatki[3]);
    $("#kreirajBday").val(podatki[4]);
    $("#kreirajVisino").val(podatki[5]);
    $("#kreirajTezo").val(podatki[6]);
    $("#kreirajTem").val(podatki[7]);
    $("#kreirajSistol").val(podatki[8]);
    $("#kreirajDiastol").val(podatki[9]);
    $("#kreirajPulse").val(podatki[10]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

});

$.getScript('https://d3js.org/d3.v3.min.js', function(){
	
    var svg = d3.select("#chart")
              	.append("svg")
              	.append("g")
              
              svg.append("g")
              	.attr("class", "slices");
              svg.append("g")
              	.attr("class", "labels");
              svg.append("g")
              	.attr("class", "lines");
              	
              	var shape = document.getElementsByTagName("svg")[0];
								shape.setAttribute("viewBox", "-100 -50 740 740"); 
              
              var width = $("#chart").width(),
                  height = $("#chart").height(),
              	radius = Math.min(width, height) / 2;
              	
              
              var pie = d3.layout.pie()
              	.sort(null)
              	.value(function(d) {
              		return d.value;
              	});
              
              var arc = d3.svg.arc()
              	.outerRadius(radius * 0.8)
              	.innerRadius(radius * 0.4);
              
              var outerArc = d3.svg.arc()
              	.innerRadius(radius * 0.9)
              	.outerRadius(radius * 0.9);
              
              svg.attr("transform", "translate(" + width / 2  + "," + height / 2  + ")");
              
              var key = function(d){ return d.data.label; };
              
              var color = d3.scale.ordinal()
              	.domain(["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"])
              	.range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);
              	
              var dcolor = d3.scale.ordinal()
              	.domain(["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"])
              	.range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);
              
              
              function randomData (){
              	var labels = color.domain();
              	return	labels.map(function(label, index){
              		//console.log(index);
              		//console.log(labels);
              		return { label: label, value: Math.random()}
              	});
              }
              
              function makeData (deaths){
              	var dcolor = d3.scale.ordinal()
              		.domain(["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]);
              	var labelsD = dcolor.domain();
              	console.log(color.domain());
            	console.log(labelsD);
              	return	labelsD.map(function(label, index){
              		console.log(index);
              		return { label: deaths[index].label, value: deaths[index].value};
              	});
              }
              
              d3.select(".randomize").on("click", function(){
              	skrijElement();
              	setTimeout(function(){
        			pokaziElement();
    			}, 2000);
              		najdiPovzrociteljeSmrti(cntry, function(topSmrti){
              			setTimeout(function(){
              				console.log(topSmrti);
              				change(makeData(topSmrti));
              			}, 1500);
              		});
              });
              
              change(randomData());
              /*
              d3.select(".randomize")
              	.on("click", function(){
              		change(randomData());
              	});
              */
              
              function change(data) {
              	console.log(data);
              
              	/* ------- PIE SLICES -------*/
              	var slice = svg.select(".slices").selectAll("path.slice")
              		.data(pie(data), key);
              
              	slice.enter()
              		.insert("path")
              		.style("fill", function(d) { return color(d.data.label); })
              		.attr("class", "slice");
              
              	slice		
              		.transition().duration(2000)
              		.attrTween("d", function(d) {
              			this._current = this._current || d;
              			var interpolate = d3.interpolate(this._current, d);
              			this._current = interpolate(0);
              			return function(t) {
              				return arc(interpolate(t));
              			};
              		})
              
              	slice.exit()
              		.remove();
              
              	/* ------- TEXT LABELS -------*/
              
              	var text = svg.select(".labels").selectAll("text")
              		.data(pie(data), key);
              
              	text.enter()
              		.append("text")
              		.attr("dy", ".35em")
              		.text(function(d) {
              			return d.data.label;
              		});
              	
              	function midAngle(d){
              		return d.startAngle + (d.endAngle - d.startAngle)/2;
              	}
              
              	text.transition().duration(1000)
              		.attrTween("transform", function(d) {
              			this._current = this._current || d;
              			var interpolate = d3.interpolate(this._current, d);
              			this._current = interpolate(0);
              			return function(t) {
              				var d2 = interpolate(t);
              				var pos = outerArc.centroid(d2);
              				pos[0] = radius * (midAngle(d2) < Math.PI ? 1 : -1);
              				return "translate("+ pos +")";
              			};
              		})
              		.styleTween("text-anchor", function(d){
              			this._current = this._current || d;
              			var interpolate = d3.interpolate(this._current, d);
              			this._current = interpolate(0);
              			return function(t) {
              				var d2 = interpolate(t);
              				return midAngle(d2) < Math.PI ? "start":"end";
              			};
              		});
              
              	text.exit()
              		.remove();
              
              	/* ------- SLICE TO TEXT POLYLINES -------*/
              
              	var polyline = svg.select(".lines").selectAll("polyline")
              		.data(pie(data), key);
              	
              	polyline.enter()
              		.append("polyline");
              
              	polyline.transition().duration(1000)
              		.attrTween("points", function(d){
              			this._current = this._current || d;
              			var interpolate = d3.interpolate(this._current, d);
              			this._current = interpolate(0);
              			return function(t) {
              				var d2 = interpolate(t);
              				var pos = outerArc.centroid(d2);
              				pos[0] = radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
              				return [arc.centroid(d2), outerArc.centroid(d2), pos];
              			};			
              		});
              	
              	polyline.exit()
              		.remove();
              };
});